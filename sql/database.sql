-- 1 up
create table "register" (
    "id" serial not null,
    "token" varchar(18),
    "registered" timestamp(0) null default now(),
    "ip_public" inet not null,
    "ip_private" inet not null,
    "name" text,
    "data" text,
    primary key("id")
);
-- 1 down
drop table "register";
