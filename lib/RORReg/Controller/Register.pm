package RORReg::Controller::Register;
use Mojo::Base 'Mojolicious::Controller';
use JSON;
use Mojo::UserAgent;

# This action will render a template
sub index {
    my $c  = shift;
    my $ua = Mojo::UserAgent->new;

    my $args      = $c->req->json();
    my $ip_public = $c->req->headers->header('x-forwarded-for');

    my %values = (
        token      => $args->{token},
        ip_public  => $ip_public,
        ip_private => $args->{ip_private},
        name       => $args->{name},
        data       => encode_json($args->{data} || {}),
        registered => \'now()',
    );

    eval {
        my $exists = $c->pg->db->select('register', ['id'], {
            -or => {
                token     => $args->{token},
                ip_public => $ip_public,
             }
        });

        if ( $exists->rows ) {
            $c->pg->db->update('register', \%values, { id => $exists->hash->{id} } );
        }
        else {
            $c->pg->db->insert('register', { %values } );
        }

        if ( $ENV{FORWARD_TO} ) {
            delete $values{registered};
            $values{data} = $args->{data};
            $ua->post($ENV{FORWARD_TO} => json => \%values);
        }
    };


    if ( $@ ) {
        $c->app->log->error($@);
        $c->render( json => { rc => 1, error => $@ } );
    }
    else {
        $c->render( json => { rc => 0 } );
    }

}

1;
