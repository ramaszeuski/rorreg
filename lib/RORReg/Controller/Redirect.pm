package RORReg::Controller::Redirect;
use Mojo::Base 'Mojolicious::Controller';

use utf8;

# This action will render a template
sub index {
    my $c = shift;

    my $exists = $c->pg->db->select('register',
        ['ip_private'],
        {
            ip_public => $c->req->headers->header('x-forwarded-for'),
        }
    );

    if ( $exists->rows ) {
        my $system = $exists->hash;
        $c->res->code(302);
        $c->redirect_to("http://$system->{ip_private}");
    }
    else {
        $c->render( 'not_found' );
    }
}

1;
