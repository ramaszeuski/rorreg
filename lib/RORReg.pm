package RORReg;
use Mojo::Base 'Mojolicious';
use Mojo::Pg;

# This method will run once at server start
sub startup {
    my $self = shift;

    $self->helper( pg => sub { return Mojo::Pg->new($ENV{DB_DSN})});
    $self->pg->migrations->from_file('sql/database.sql')->migrate;

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->post('/')->to('register#index');
    $r->get('/')->to('redirect#index');
}

1;
