FROM alpine:latest

RUN apk update && apk add \
    wget make \
    perl-app-cpanminus \
    perl-json \
    perl-dbd-pg

RUN cpanm \
    Mojolicious \
    Mojo::Pg

ADD . /opt/rorreg
WORKDIR /opt/rorreg

USER nobody
EXPOSE 3000
CMD /opt/rorreg/script/rorreg daemon -c 3000
